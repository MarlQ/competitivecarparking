using System.Dynamic;
using System.Security;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalLot : MonoBehaviour
{
    public ParkingLotManager manager;
    public bool occupied = false;

    private void OnTriggerEnter(Collider other)
    {
        if(!occupied) {
            if(other.tag == "Car") {
                gameObject.tag = "OccupiedGoal";
                occupied = true;
                this.transform.Find("Visualizer").GetComponent<Renderer>().material.color = new Color(0f, 0.8f, 0f, 1f);
                manager.GoalLotEntered(other.GetComponent<CarController>(), this);
            }
        }
    }

}
