using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Policies;

public class PlayerController : MonoBehaviour
{
   
    public Rigidbody rb;

    float horizontal;
    float vertical;

    public float maxSpeed;
    public float acceleration;
    public float steering;
    //public Transform Goal;

    public bool parked = false;

    private float currentSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }

    public void setParked() {
        transform.gameObject.tag = "Obstacle";
        this.parked = true;
        rb.velocity = Vector3.zero;
        horizontal = 0;
        vertical = 0;
    }

    private void Update()
    {
        //print((new Vector2(rb.velocity.x, rb.velocity.z)).magnitude);
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
    }

    private void FixedUpdate()
     {         
         // Calculate speed from input and acceleration (transform.up is forward)
         Vector3 forward = Vector3.Scale(transform.forward, new Vector3(1,0,1));
         Vector3 speed = forward * (vertical * acceleration);
         if(vertical < 0 ) { speed = speed * 0.75f;}
         rb.AddForce(speed);
 
         // Create car rotation
         float direction = Vector3.Dot(rb.velocity, forward);
         if (direction >= 0.0f)
         {
            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0,1,0) * horizontal * steering * (rb.velocity.magnitude / maxSpeed));
            rb.MoveRotation(rb.rotation * deltaRotation);
         }
         else
         {
            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0,1,0) * -horizontal * steering * (rb.velocity.magnitude / maxSpeed));
            rb.MoveRotation(rb.rotation * deltaRotation);
         }
 
         // Change velocity based on rotation
         float driftForce = Vector3.Dot(rb.velocity, rb.GetRelativePointVelocity(Vector3.left)) * 2.0f;
         Vector3 relativeForce = Vector3.right * driftForce;
         rb.AddForce(rb.GetRelativePointVelocity(relativeForce));
 
         // Force max speed limit
         if (rb.velocity.magnitude > maxSpeed)
         {
             rb.velocity = rb.velocity.normalized * maxSpeed;
         }
         currentSpeed = rb.velocity.magnitude;
     }
}
