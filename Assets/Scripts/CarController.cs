using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Policies;

public class CarController : Agent
{
    public ParkingLotManager manager; 
    public Rigidbody rb;
    BehaviorParameters behavior;

    float horizontal;
    float vertical;

    public float maxSpeed;
    public float acceleration;
    public float steering;

    
    //public Transform Goal;

    public bool parked = false;

    private float currentSpeed;

    public int collisions = 0;

    public GoalLot Goal;
    // Start is called before the first frame update
    public override void Initialize()
    {
        rb = GetComponent<Rigidbody>(); 
        behavior = GetComponent<BehaviorParameters>();
    }

    public void setParked() {
        transform.gameObject.tag = "Obstacle";
        this.parked = true;
        rb.velocity = Vector3.zero;
        horizontal = 0;
        vertical = 0;
    }
    public void getGoalDistanceReward() {
        float distanceToTarget = Vector3.Distance(Goal.transform.position, this.transform.position);
        AddReward(Mathf.Min(1/distanceToTarget, 1) * 1f);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        // Target and Agent positions
        sensor.AddObservation(Goal.transform.position - this.transform.position);
        sensor.AddObservation(Vector3.Distance(Goal.transform.position, this.transform.position));
        //sensor.AddObservation(Goal.localPosition);
        //sensor.AddObservation(this.transform.localPosition);

        // Agent velocity
        sensor.AddObservation(rb.velocity.x);
        sensor.AddObservation(rb.velocity.z);

        sensor.AddObservation(parked);

        List<float> carList = manager.getClosestCars(this,Goal);
        float[] closestExists = {0,0,0,0};
        float[] closest = {40,40,40,40};

        for(int i = 0; i < carList.Count; i++) {
            if(i >= closest.Length) {break;}
            closest[i] = carList[i];
            closestExists[i] = 1;
        }
        sensor.AddObservation(closest);
        sensor.AddObservation(closestExists);
        //sensor.AddObservation(manager.maxSteps - manager.currentStep);

        sensor.AddObservation(Vector3.Angle(transform.forward, Goal.transform.forward));
    }

    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        if(parked) {return;}




        float hor = Mathf.Clamp(actionBuffers.ContinuousActions[0], -1f, 1f);
        float ver = Mathf.Clamp(actionBuffers.ContinuousActions[1], -1f, 1f); 

        if(Mathf.Abs(hor) < 0.2) {
            horizontal = 0.0f;
        }
        else if(hor > 0) {
            horizontal = ScaleAction(hor, 0.6f, 1.0f);
        }
        else if(hor < 0) {
            horizontal = ScaleAction(hor, -1.0f, -0.6f);
        }

        if(Mathf.Abs(ver) < 0.2) {
            vertical = 0.0f;
        }
        else if(ver > 0) {
            vertical = ScaleAction(ver, 0.6f, 1.0f);
        }
        else if(ver < 0) {
            vertical = ScaleAction(ver, -1.0f, -0.6f);
        }

        /* 
        if(actionBuffers.ContinuousActions[1] < 0 ) {
            if(actionBuffers.ContinuousActions[1] < 0.5 ) {
                vertical = actionBuffers.ContinuousActions[1];
            }
            else {
                vertical = 0;
            }
        }
        else {
            if(actionBuffers.ContinuousActions[1] > 0.5 ) {
                vertical = actionBuffers.ContinuousActions[1];
            }
            else {
                vertical = 0;
            }
        }
        
        
        horizontal = Mathf.Clamp(actionBuffers.ContinuousActions[0], -1f, 1f);
        //vertical = Mathf.Clamp(actionBuffers.ContinuousActions[1], -1f, 1f); 
        */

        // Get the action index for movement
        /* if(!(behavior.IsInHeuristicMode())) {
            int movement = actionBuffers.DiscreteActions[0];
            // Get the action index for jumping
            int turning = actionBuffers.DiscreteActions[1];

            // Look up the index in the movement action list:
            if (movement == 0) { vertical = 0; }
            else if (movement == 1) { vertical = 1; }
            else if (movement == 2) { vertical = -1; }

            if (turning == 0) { horizontal = 0; }
            else if (turning == 1) { horizontal = 1; }
            else if (turning == 2) { horizontal = -1; }
        } */
        
        
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {

        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[0] = Input.GetAxis("Horizontal");
        continuousActionsOut[1] = Input.GetAxis("Vertical");

        /* horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical"); */
        /**
        var discreteActionsOut = actionsOut.DiscreteActions;
        int hor = 0;
        int ver = 0;
    
        if(Input.GetAxis("Vertical") == 0) {ver=0;}
        else if(Input.GetAxis("Vertical") < 0) {ver = 2;}
        else ver = 1;

        if(Input.GetAxis("Horizontal") == 0) {hor=0;}
        else if(Input.GetAxis("Horizontal") < 0) {hor = 2;}
        else hor = 1;
        
        discreteActionsOut[0] = ver;
        discreteActionsOut[1] = hor;**/
    }

    private void FixedUpdate()
     {         

         // Calculate speed from input and acceleration (transform.up is forward)
         Vector3 forward = Vector3.Scale(transform.forward, new Vector3(1,0,1));
         Vector3 speed = forward * (vertical * acceleration);
         if(vertical < 0 ) { speed = speed * 0.75f;}
         rb.AddForce(speed);
 
         // Create car rotation
         float direction = Vector3.Dot(rb.velocity, forward);
         if (direction >= 0.0f)
         {
            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0,1,0) * horizontal * steering * (rb.velocity.magnitude / maxSpeed));
            rb.MoveRotation(rb.rotation * deltaRotation);
         }
         else
         {
            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0,1,0) * -horizontal * steering * (rb.velocity.magnitude / maxSpeed));
            rb.MoveRotation(rb.rotation * deltaRotation);
         }
 
         // Change velocity based on rotation
         float driftForce = Vector3.Dot(rb.velocity, rb.GetRelativePointVelocity(Vector3.left)) * 2.0f;
         Vector3 relativeForce = Vector3.right * driftForce;
         rb.AddForce(rb.GetRelativePointVelocity(relativeForce));
 
         // Force max speed limit
         if (rb.velocity.magnitude > maxSpeed)
         {
             rb.velocity = rb.velocity.normalized * maxSpeed;
         }
         currentSpeed = rb.velocity.magnitude;

         /* if (this.transform.localPosition.y < -1.0f) { // Fell out of map (should not be possible)
            AddReward(-1.0f);
            //manager.gameOverRatings();
            //EndEpisode();
            //manager.resetGame();
            //manager.negativeSignal();
        } */
     }

     void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Obstacle")) {
            AddReward(-0.05f);
            collisions++;
        }
    }
}
