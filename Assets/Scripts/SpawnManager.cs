using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    [SerializeField] int rowCount;
    [SerializeField] int rowWidth;
    [SerializeField] int columnCount;

    [SerializeField] float baseDist;
    [SerializeField] float rowDist;
    [SerializeField] float columnDist;

    [SerializeField] GameObject ParkingLotManagerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        for(int row = 0; row < rowCount; row++) {
            for(int column = 0; column < columnCount; column++) {
                for(int i = 0; i < rowWidth; i++) {
                    Instantiate(ParkingLotManagerPrefab, new Vector3(i*baseDist, column*columnDist, row*rowDist), Quaternion.Euler(0f, 0f, 0f));
                }
            }
        }
    }
}
