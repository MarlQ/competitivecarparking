
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;

public class ParkingLotManager : MonoBehaviour
{
    int rowWidth = 8;
    int rowCount = 6;
    [SerializeField] GameObject ParkingLotPrefab;
    [SerializeField] GameObject GoalLotPrefab;
    [SerializeField] GameObject[] CarParkedPrefabs;
    [SerializeField] GameObject GroundPrefab;
    [SerializeField] GameObject WallPrefab;
    [SerializeField] GameObject CarPrefab;
    //[SerializeField] CarController Car;

    [SerializeField] List<CarController> cars;
    [SerializeField] List<GoalLot> lots;
    GameObject ground;


    float lotWidth = 1.85f;
    float lotGap = 8.0f;
    float lotDepth = 3.4f;
    float groundPadding = 25.0f;
    float wallWidth = 1.0f;
    float wallHeight = 1.5f;

    float rowGapMinor = .5f;
    float rowWallHeight = .8f;
    int rowWallChance = 50;

    int carAmount = 4;

    float games = 0;
    float completedGames = 0;

    StatsRecorder statsRecorder;

    public float maxSteps;
    public float currentStep = 0;


    public List<float> getClosestCars(CarController self, GoalLot goal) {
        float epsilon = 40.0f;
        List<float> surrounding = new List<float>();
        foreach(CarController car in cars) {
            if(car.transform.position != self.transform.position) {
                float dist = Vector3.Distance(goal.transform.position, car.transform.position);
                if(dist < epsilon) {
                    surrounding.Add(dist);
                }
            } 
        }
        return surrounding;
    }

    /** Finds the closest non-occupied parking lot from a position **/
    public GoalLot getClosestGoal(Vector3 position) {
        GoalLot closest = lots[0];
        float closestDist = Mathf.Infinity;
        foreach(GoalLot lot in lots) { 
            if(!lot.occupied) {
                Vector3 lotPos = lot.transform.position;
                float dist = Vector3.Distance(lotPos, position);
                if(dist < closestDist) {
                    closest = lot;
                    closestDist = dist;
                }
            }
        }
        return closest;
    }

    bool allLotsOccupied() {
        bool occupied = true;
        foreach(GoalLot lot in lots) {
            if(!lot.occupied) { occupied = false; }
        }
        return occupied;
    }

    bool allCarsParked() {
        bool parked = true;
        foreach(CarController car in cars) {
            if(!car.parked) { parked = false;}
        }
        return parked;
    }

    void Start() {
        statsRecorder = Academy.Instance.StatsRecorder;
        resetGame();
    }
    public void resetGame() {
        currentStep = 0;
        destroyParkingLot();
        generateParkingLot();
    }

    private void FixedUpdate() {
        currentStep++;
        foreach(CarController car in cars) {
            if(!car.parked) { // Punish non=parked cars for time
                car.AddReward(-1f / maxSteps);
            }
        }

        if(currentStep > maxSteps) { // Game over due to time
            int filledLots = 0;
            foreach(GoalLot lot in lots) {
                if(lot.occupied) {
                    filledLots++;
                }
            }
            statsRecorder.Add("Game/Filled lots ratio", filledLots / lots.Count, StatAggregationMethod.Average);
            int collisions = 0;
            foreach(CarController c in cars) {
                collisions += c.collisions;
            }
            statsRecorder.Add("Game/Collisions", collisions);
            gameOverRatings(true);
            resetGame();
            negativeSignal();
         }
    }

    public void gameOverRatings(bool interrupted) {
        bool allOccupied = allLotsOccupied();
        foreach(CarController c in cars) {
            if(!c.parked) {
                //c.AddReward(-0.5f);
                if(!allOccupied) { // Consolation price on time-out (can't happen otherwise)
                    c.Goal = getClosestGoal(c.transform.position);
                    c.getGoalDistanceReward();
                }
            }
            if(interrupted) {
                c.EpisodeInterrupted();
            }  
            else {
                c.EndEpisode();
            }
            
        }
    }

    float rateParking(CarController car, GoalLot goal) {
        float angle = Vector3.Angle(car.transform.forward, goal.transform.forward);
        float angleFactor = Mathf.Pow(( Mathf.Abs(90.0f - angle) / 90 ), 0.4f); // Maps 0 to 90 degree difference, to 1 to 0 points

        float maxVelocity = 18.0f;
        float goodVelocity = 10.0f;
        Rigidbody rb = car.rb;
        float velocity = (new Vector2(rb.velocity.x, rb.velocity.z)).magnitude;
        float velFactor = 1.0f;
        if(velocity < goodVelocity) {
            velFactor = 1.0f;
        }
        else if(velocity < maxVelocity) {
            velFactor = Mathf.Pow(( Mathf.Abs(maxVelocity - velocity) / maxVelocity ), 0.7f);
        }
        else { 
            velFactor = 0.0f;
        }
        velFactor = 1.0f;

        float timeFactor = 0.8f;

        if(currentStep < maxSteps/4) {
            timeFactor = 1.0f;
        }
        else if(currentStep < maxSteps/2) {
            timeFactor = 0.9f;
        }
        /* print("Velocity: " + velocity);
        print("Velocity factor: " + velFactor);
        print("Angle factor: " + angleFactor);
        print("Total: " + 1.0f * angleFactor * velFactor); */
        statsRecorder.Add("Game/Parking rating", 1.0f * angleFactor * velFactor * timeFactor);
        return 1.0f * angleFactor * velFactor * timeFactor;   
    }

    virtual public void GoalLotEntered(CarController car, GoalLot goalLot) {
        car.AddReward(rateParking(car,goalLot));
        car.setParked();
        
        
        if(allLotsOccupied() || allCarsParked()) { // Game over
            int filledLots = 0;
            foreach(GoalLot lot in lots) {
                if(lot.occupied) {
                    filledLots++;
                }
            }
            statsRecorder.Add("Game/Filled lots ratio", filledLots / lots.Count, StatAggregationMethod.Average);
            int collisions = 0;
            bool carsParked = allCarsParked();
            foreach(CarController c in cars) {
                collisions += c.collisions;
                if(carsParked) {
                    car.AddReward(0.2f);
                }
            }
            statsRecorder.Add("Game/Collisions", collisions);
            gameOverRatings(false);
            resetGame();
            positiveSignal();
        }
        else { // Re-assign goals
            foreach(CarController c in cars) {
                if(!c.parked) {
                    c.Goal = getClosestGoal(c.transform.position);
                }
            }
        }
        
    }

    public void positiveSignal() {
        games++;
        completedGames++;
        statsRecorder.Add("Game/Completed games total ratio", completedGames / games, StatAggregationMethod.MostRecent);
        statsRecorder.Add("Game/Completed games short-term ratio", 1, StatAggregationMethod.Average);
        
        ground.GetComponent<Renderer>().material.color = new Color(0f, 0.8f, 0f, 1f);
        Invoke("ResetColor", 0.5f);
    }
    public void negativeSignal() {
        games++;
        statsRecorder.Add("Game/Completed games total ratio", completedGames / games, StatAggregationMethod.MostRecent);
        statsRecorder.Add("Game/Completed games short-term ratio", 0, StatAggregationMethod.Average);
        ground.GetComponent<Renderer>().material.color = new Color(0.8f, 0f, 0f, 1f);
        Invoke("ResetColor", 0.5f);
    }

    private void ResetColor() {
        ground.GetComponent<Renderer>().material.color = new Color(0.03773582f, 0.03773582f, 0.03773582f, 0.03773582f);
    }

    public void destroyParkingLot() {
        foreach(Transform child in this.transform)
        {   
            child.gameObject.SetActive(false);
            Destroy(child.gameObject);
        }
        cars.Clear();
        lots.Clear();
    }

    private void generateBlock(float blockOffset, List<int> goalLots) {
        float rowOffset = 0.0f;
        float rowOffsetTotal = (rowCount + 1) / 2 * lotGap + (rowCount + 1) / 2 * rowGapMinor;
        float totalDepth = (rowCount-1) * lotDepth + rowOffsetTotal;
        float totalWidth = (rowWidth-1) * lotWidth;
        for(int i = 0; i < rowCount; i++) {
            Quaternion rot;

            if(i % 2 == 0) {
                rot = Quaternion.Euler(0f, 0f, 0f);
                rowOffset += rowGapMinor;
                if(Random.Range(0,100) < rowWallChance) {
                    // Generate wall between rows
                    GameObject wall = Instantiate(WallPrefab, this.transform.position + new Vector3(blockOffset, 0, i*lotDepth + rowOffset - totalDepth/2 - rowGapMinor/2 - lotDepth/2), Quaternion.Euler(0f, 0f, 0f), this.transform);
                    wall.transform.localScale = new Vector3(totalWidth, rowWallHeight, rowGapMinor);
                }
            }
            else {
                rowOffset += lotGap;
                rot = Quaternion.Euler(0f, 180f, 0f);
            }

            // Generate row of parking lots
            for(int j = 0; j < rowWidth; j++) {

                Vector3 pos = this.transform.position + new Vector3(j*lotWidth - totalWidth/2 + blockOffset, 0, i*lotDepth + rowOffset - totalDepth/2);

                if(goalLots.Contains(i*rowWidth + j)) {
                    GameObject goal = Instantiate(GoalLotPrefab, pos, rot, this.transform);
                    goal.GetComponent<GoalLot>().manager = this;
                    /* foreach(CarController car in cars) {
                        car.Goal = goal.transform;
                    } */
                    lots.Add(goal.GetComponent<GoalLot>());
                }
                else {
                    var a = Instantiate(ParkingLotPrefab, pos, rot, this.transform);
                    var prefab = CarParkedPrefabs[Random.Range(0,CarParkedPrefabs.Length)];
                    Instantiate(prefab, pos, rot, a.transform);
                }
            }
        }
    }
    
    public void generateParkingLot()
    {
        EnvironmentParameters m_ResetParams = Academy.Instance.EnvironmentParameters;

        int mapSize = (int) m_ResetParams.GetWithDefault("map_size", 6);
        int goalLotCount = Mathf.Max(1, Random.Range(carAmount-2, carAmount+2));
        int carParameter = (int) m_ResetParams.GetWithDefault("cars", 4);
        carAmount = Random.Range(1,carParameter+1);

        switch((int) m_ResetParams.GetWithDefault("difficulty", 8)) {
            case 1:
                mapSize = 1;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = 1;
                goalLotCount = 2;
                carAmount = 1;
                maxSteps = 2000;
                break;
            case 2:
                mapSize = 2;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = Random.Range(1,mapSize+1);
                goalLotCount = 2;
                carAmount = 1;
                maxSteps = 1000;
                break;
            case 3:
                mapSize = 3;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = Random.Range(1,mapSize+1);
                goalLotCount = 2;
                carAmount = 1;
                maxSteps = 2000;
                break;
            case 4:
                mapSize = 4;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = Random.Range(1,mapSize+1);
                goalLotCount = 3;
                carAmount = 1;
                maxSteps = 2000;
                break;
            case 5:
                mapSize = 5;
                rowWidth = Random.Range(mapSize+4,mapSize+6);
                rowCount = Random.Range(mapSize,mapSize+4);
                goalLotCount = 5;
                carAmount = 1;
                maxSteps = 2000;
                break;
            case 6:
                mapSize = 2;
                rowWidth = Random.Range(mapSize,mapSize+6);
                rowCount = Random.Range(mapSize,mapSize+1);
                goalLotCount = 5;
                carAmount = Random.Range(1,3);
                maxSteps = 3000;
                break;
            case 7:
                mapSize = 4;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = Random.Range(mapSize,mapSize+1);
                goalLotCount = 5;
                carAmount = Random.Range(1,3);
                maxSteps = 3000;
                break;
            case 8:
                mapSize = 4;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = Random.Range(mapSize,mapSize+1);
                goalLotCount = 5;
                carAmount = Random.Range(1,4);
                maxSteps = 4000;
                break;
            case 9:
                mapSize = 4;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = Random.Range(mapSize,mapSize+1);
                goalLotCount = 3;
                carAmount = Random.Range(1,4);
                maxSteps = 3000;
                break;
            case 10:
                mapSize = 4;
                rowWidth = Random.Range(mapSize,mapSize+5);
                rowCount = Random.Range(mapSize,mapSize+4);
                goalLotCount = 4;
                carAmount = Random.Range(1,5);
                maxSteps = 4000;
                break;
            default:
                break;
        }








        // Parameters        
        for(int i = 0; i < carAmount; i++) {
            GameObject car = Instantiate(CarPrefab, this.transform.position, Quaternion.Euler(0f, 0f, 0f), this.transform);
            CarController carController = car.GetComponent<CarController>();
            carController.manager = this;
            cars.Add(carController);
        }
        

        // MapSize goes from 1 to 4
        

        /* rowWidth = Random.Range(mapSize,mapSize+5);
        rowCount = Random.Range(1,mapSize+1); */
        int blockCountX = 1;
        int blockCountY = 1;
        float blockGap = 5.0f;

        /* if(mapSize < 4) {
            rowWidth = Random.Range(mapSize,mapSize+5);
            rowCount = Random.Range(1,mapSize+1);
        }
        else {
            rowWidth = Random.Range(mapSize+4,mapSize+8);
            rowCount = Random.Range(mapSize,mapSize+4);
        } */

        

        // Create parking lots
        int lotCount = rowWidth * rowCount;
        float rowOffsetTotal = (rowCount + 1) / 2 * lotGap + (rowCount + 1) / 2 * rowGapMinor;
        float totalDepth = (rowCount-1) * lotDepth + rowOffsetTotal;
        float totalWidth = (rowWidth-1) * lotWidth;
        
        int goalLot = Random.Range(0,lotCount);
       
        List<int> goalLots = new List<int>();


        for(int i = 0; i < goalLotCount; i++) {
            int random = Random.Range(0,lotCount);
            int maxCountRand = 5; // Max count before it starts incrementing instead of choosing randomly
            int maxCountTotal = 10; // Max count before it gives up entirely
            bool exists = false;

            int tries = 0;
            do { // Assign new random value for goal lot
                exists = false;
                if(tries > maxCountTotal) {
                    break;
                }
                for(int j = 0; j < i; j++) {
                    if(goalLots[j] == random) {
                        exists = true;
                    }
                }
                if(exists) {
                    if(tries < maxCountRand) {
                        random = Random.Range(0,lotCount);
                    }
                    else {
                        random = Mathf.Clamp(random+1, 0, lotCount-1);
                    }
                }
                tries++;
            } while(exists);
            goalLots.Add(random);
        }


        for(int bx = 0; bx < blockCountX; bx++) {
            for(int by = 0; by < blockCountY; by++) {
                float blockOffset = bx * rowWidth * lotWidth + bx * blockGap;
                generateBlock(blockOffset, goalLots);
            }
        }


        // Adjust position
        //this.transform.position = this.transform.position - new Vector3(totalDepth/2, 0, totalWidth/2);

        // Creating floor
        float groundDepth = totalDepth + groundPadding;
        float groundWidth = totalWidth * blockCountX + groundPadding + (blockCountX-1)*blockGap;
        ground = Instantiate(GroundPrefab, this.transform.position + new Vector3(0, -0.05f, 0), Quaternion.Euler(0f, 0f, 0f), this.transform);
        ground.transform.localScale = new Vector3(groundWidth, 0.1f, groundDepth);

        // Create walls
        GameObject wallLeft = Instantiate(WallPrefab, this.transform.position + new Vector3(-groundWidth/2 - wallWidth/2, 0, 0), Quaternion.Euler(0f, 0f, 0f), this.transform);
        wallLeft.transform.localScale = new Vector3(wallWidth, wallHeight, groundDepth);

        GameObject wallRight = Instantiate(WallPrefab, this.transform.position + new Vector3(groundWidth/2 + wallWidth/2, 0, 0), Quaternion.Euler(0f, 0f, 0f), this.transform);
        wallRight.transform.localScale = new Vector3(wallWidth, wallHeight, groundDepth);

        GameObject wallBottom = Instantiate(WallPrefab, this.transform.position + new Vector3(0, 0, -groundDepth/2 - wallWidth/2), Quaternion.Euler(0f, 0f, 0f), this.transform);
        wallBottom.transform.localScale = new Vector3(groundWidth + wallWidth*2, wallHeight, wallWidth);

        GameObject wallTop = Instantiate(WallPrefab, this.transform.position + new Vector3(0, 0, groundDepth/2 + wallWidth/2), Quaternion.Euler(0f, 0f, 0f), this.transform);
        wallTop.transform.localScale = new Vector3(groundWidth + wallWidth*2, wallHeight, wallWidth);


        // Set car spawn positions
        Vector3[] spawnPositions = {  
            new Vector3(totalWidth/2 + groundPadding/4, 0, totalDepth/2 + groundPadding/4), // Top right
            new Vector3(totalWidth/2 + groundPadding/4, 0, 0), // Right
            new Vector3(-totalWidth/2 - groundPadding/4, 0, totalDepth/2 + groundPadding/4), // Top left
            new Vector3(-totalWidth/2 - groundPadding/4, 0, 0), // left
            new Vector3(+totalWidth/2 + groundPadding/4, 0, -totalDepth/2 - groundPadding/4), // Bottom right
            new Vector3(0, 0, totalDepth/2 + groundPadding/4), // Top
            new Vector3(-totalWidth/2 - groundPadding/4, 0, -totalDepth/2 - groundPadding/4), // Bottom Left
            new Vector3(0, 0, -totalDepth/2 - groundPadding/4), // Bottom
        };

        Quaternion[] spawnRotations = {
            Quaternion.Euler(0f, 180f, 0f), // Top right
            Quaternion.Euler(0f, -90f, 0f), // Right
            Quaternion.Euler(0f, 180f, 0f), // Top left
            Quaternion.Euler(0f, 90f, 0f), // left
            Quaternion.Euler(0f, 0f, 0f), // Bottom right
            Quaternion.Euler(0f, 180f, 0f), // Top
            Quaternion.Euler(0f, 0f, 0f),
            Quaternion.Euler(0f, 0f, 0f) // Bottom
        };
        List<int> positions = new List<int>() {0,1,2,3,4,5,6};

        for(int i = 0; i < carAmount; i++) {
            int ran = Random.Range(0,positions.Count);
            int pos = positions[ran];
            positions.RemoveAt(ran);

            Vector3 spawnPos = spawnPositions[pos];
            Quaternion spawnRot = spawnRotations[pos];
            CarController car = cars[i];
            car.transform.rotation = spawnRot;
            car.transform.position = this.transform.position + spawnPos;
            car.Goal = getClosestGoal(car.transform.position);
        }
    }
}
