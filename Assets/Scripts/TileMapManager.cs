using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMapManager : ParkingLotManager
{

    CarController[] cars;
    GoalLot[] lots;

    new public List<float> getClosestCars(CarController self, GoalLot goal) {
        float epsilon = 40.0f;
        List<float> surrounding = new List<float>();
        foreach(CarController car in cars) {
            if(car.transform.position != self.transform.position) {
                float dist = Vector3.Distance(goal.transform.position, car.transform.position);
                if(dist < epsilon) {
                    surrounding.Add(dist);
                }
            } 
        }
        return surrounding;
    }

    /** Finds the closest non-occupied parking lot from a position **/
    new public GoalLot getClosestGoal(Vector3 position) {
        GoalLot closest = lots[0];
        float closestDist = Mathf.Infinity;
        foreach(GoalLot lot in lots) { 
            if(!lot.occupied) {
                Vector3 lotPos = lot.transform.position;
                float dist = Vector3.Distance(lotPos, position);
                if(dist < closestDist) {
                    closest = lot;
                    closestDist = dist;
                }
            }
        }
        return closest;
    }

    bool allLotsOccupied() {
        bool occupied = true;
        foreach(GoalLot lot in lots) {
            if(!lot.occupied) { occupied = false; }
        }
        return occupied;
    }

    bool allCarsParked() {
        bool parked = true;
        foreach(CarController car in cars) {
            if(!car.parked) { parked = false;}
        }
        return parked;
    }

    private void FixedUpdate() {
        currentStep++;

        if(currentStep > maxSteps) { // Game over due to time
         }
    }

    override public void GoalLotEntered(CarController car, GoalLot goalLot) {
        car.setParked();
        
        
        if(allLotsOccupied() || allCarsParked()) { // Game over
            
        }
        else { // Re-assign goals
            foreach(CarController c in cars) {
                if(!c.parked) {
                    c.Goal = getClosestGoal(c.transform.position);
                }
            }
        }
        
    }


    // Start is called before the first frame update
    void Start()
    {
        cars = Object.FindObjectsOfType<CarController>();
        lots = Object.FindObjectsOfType<GoalLot>(); 

        for(int i = 0; i < cars.Length; i++) {
            CarController car = cars[i];
            car.Goal = getClosestGoal(car.transform.position);
            car.manager = this;
        }
        for(int i = 0; i < lots.Length; i++) {
            GoalLot lot = lots[i];
            lot.manager = this;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
